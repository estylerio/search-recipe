import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { Formulario } from './formulario/formulario.component';
import { Lista } from './lista/lista.component';
import { AppRoutingModule } from './/app-routing.module';
import { DeshboardComponent } from './deshboard/deshboard.component';

@NgModule({
  declarations: [
    AppComponent,
    Formulario,
    Lista,
    DeshboardComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
