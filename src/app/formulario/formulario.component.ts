import { Component, OnInit } from '@angular/core';
import { MiServicio } from '../mi-servicio.service';

@Component({
  selector: 'formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})

export class Formulario implements OnInit {

  public listado;
  public hayRecetas = false;
  public buscar = '';

  public haBuscado = false;

  constructor(private miServicio: MiServicio) { }

  ngOnInit() {
    this.getIntroducido();
  }

  getIntroducido(): void {}

  getSearchReceta(): void {
    if (this.buscar.length > 2 ) {
      this.haBuscado = true;
      this.searchRecetas(this.buscar);
    }
  }
  
  searchRecetas(recetaABuscar) {
    this.miServicio.getReceta(recetaABuscar).subscribe(response => {
      this.listado = response.results;
      if (this.listado && this.listado.length > 0 ) {
        this.hayRecetas = true;
      } else {
        this.hayRecetas = false;
      }
    });
  }

}
