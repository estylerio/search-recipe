import { TestBed, inject } from '@angular/core/testing';

import { MiServicio } from './mi-servicio.service';

describe('MiServicio', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MiServicio]
    });
  });

  it('should be created', inject([MiServicio], (service: MiServicio) => {
    expect(service).toBeTruthy();
  }));
});
