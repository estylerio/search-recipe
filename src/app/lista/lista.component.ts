import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class Lista implements OnInit {

   /**
   * @name listadoRecetas
   * @description variable pública para el listado de recetas.
   */
  @Input() public listadoRecetas;

  constructor() { }

  ngOnInit() {
  }

  openRecetaNewWindow(url) {
    window.open(url);
  }

}
