import { TestBed, inject } from '@angular/core/testing';

import { ServicioMensaje } from './servicio-mensaje.service';

describe('ServicioMensajeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServicioMensaje]
    });
  });

  it('should be created', inject([ServicioMensaje], (service: ServicioMensaje) => {
    expect(service).toBeTruthy();
  }));
});
