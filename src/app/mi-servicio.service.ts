import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class MiServicio {

  constructor(public http: HttpClient) { }

  getReceta(recetasABuscar): Observable<any> {
    const url = 'http://recipepuppyproxy.herokuapp.com/api/?q=' + recetasABuscar;
    return this.http.get(url);
  }
 
}
